# Raspberry Pi iBeacon Project.

## Description

RPi will scan surround iBeacons and measure their distance from its 


## Preparing Raspberry Pi.
    
    sudo apt-get update
    sudo apt-get upgrade

    sudo apt-get install -y libusb-dev 
    sudo apt-get install -y libglib2.0-dev --fix-missing
    sudo apt-get install -y libudev-dev
    sudo apt-get install -y libical-dev
    sudo apt-get install -y libreadline-dev
    sudo apt-get install -y libdbus-glib-1-dev
    sudo apt-get install -y bluetooth bluez blueman
    sudo apt-get install -y python-bluez
    
## Test BLE Scanner
    
    sudo python ble_scan.py
    
## Install PubNub package.
    
The latest version(4.0) does not work on the RPi, and we have to install older version
    
    sudo pip install service_identity
    sudo pip install pubnub==3.8.2
    sudo pip install rfc3339
    